﻿using UnityEngine;
using System.Collections;

public class ProjectileFollow : MonoBehaviour {

	public Transform projectile;
	public Transform farLeft;
	public Transform farRight;

	void Update ()
	{
		Vector3 newPosition = transform.position;
		newPosition.x = projectile.position.x;
		newPosition.x = Mathf.Clamp (newPosition.x, farLeft.position.x, farRight.position.x);
		transform.position = newPosition;
	}
	/*
	public Transform target;
	public float smoothing = 5f;

	Vector3 offset;

	void Start () {
		offset = transform.position - target.position;
	}
	
	void FixedUpdate()
	{
		Vector3 targetCamPos = target.position + offset;
		transform.position = Vector3.Lerp (transform.position, targetCamPos, smoothing * Time.deltaTime);
	}*/
}
